define([],function(){
  var utils = {
    extends: function(first, second){
      for(var property in second)
        second.hasOwnProperty(property)&&(first[property]=second[property]);
    },
    isIOS: function(){
      var ua = navigator.userAgent;
      var iOS = !!ua.match(/iPad/i) || !!ua.match(/iPhone/i);
      var webkit = !!ua.match(/WebKit/i);
      return iOS && webkit && !ua.match(/CriOS/i);
    },

    webglEnabled: function(){
      try {
        var canvas = document.createElement( 'canvas' );
        return !!( window.WebGLRenderingContext && (
          canvas.getContext( 'webgl' ) ||
          canvas.getContext( 'experimental-webgl' ) )
        );
      } catch ( e ) {
        return false;
      }
    }
  }
  return utils;
});


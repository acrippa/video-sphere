require.config({
  paths: {
    three: 'libs/three.min',
    dash: 'http://cdn.dashjs.org/latest/dash.all.min',
    hls: 'https://cdn.jsdelivr.net/hls.js/latest/hls.min'
  }
});

define(['app'], function(app){
  console.log("Initializing App");
  app.init({
    video: {
        src: 'http://vm2.dashif.org/livesim/testpic_2s/Manifest.mpd'
      }
    });
});
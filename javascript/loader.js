var Loader = {

  init: function(){
    this._poster = document.createElement( 'div' );
    this._poster.className = 'poster';
    this._poster.addEventListener('click', function(){
      if (app.isReady && !app.playing){
        this.style.display = 'none';
        app.start();
      }
    }, true);

    this._loader=document.createElement("div"),
    this._loader.className="loader",
    this._cube_grid=document.createElement("div"),
    this._cube_grid.className="sk-cube-grid",
    this._loader.appendChild(this._cube_grid),
    this._cube1=document.createElement("div"),
    this._cube1.className="sk-cube sk-cube1",
    this._cube_grid.appendChild(this._cube1),
    this._cube2=document.createElement("div"),
    this._cube2.className="sk-cube sk-cube2",
    this._cube_grid.appendChild(this._cube2),
    this._cube3=document.createElement("div"),
    this._cube3.className="sk-cube sk-cube3",
    this._cube_grid.appendChild(this._cube3),
    this._cube4=document.createElement("div"),
    this._cube4.className="sk-cube sk-cube4",
    this._cube_grid.appendChild(this._cube4),
    this._cube5=document.createElement("div"),
    this._cube5.className="sk-cube sk-cube5",
    this._cube_grid.appendChild(this._cube5),
    this._cube6=document.createElement("div"),
    this._cube6.className="sk-cube sk-cube6",
    this._cube_grid.appendChild(this._cube6),
    this._cube7=document.createElement("div"),
    this._cube7.className="sk-cube sk-cube7",
    this._cube_grid.appendChild(this._cube7),
    this._cube8=document.createElement("div"),
    this._cube8.className="sk-cube sk-cube8",
    this._cube_grid.appendChild(this._cube8),
    this._cube9=document.createElement("div"),
    this._cube9.className="sk-cube sk-cube9",
    this._cube_grid.appendChild(this._cube9),
    document.body.appendChild(this._poster);
    document.body.appendChild(this._loader);
    return this;
  },

  hide: function(){
    if(this._loader !== 'undefined')
        this._loader.style.display = 'none';
  }

};
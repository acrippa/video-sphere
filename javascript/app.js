define(['utils','video', 'player', 'controls'], function(utils, video, player, controls){
  var app = {
    autoplay: false,
    isReady: false,
    video: {
      src: ''
    },
    init: function(config){
      utils.extends(this, config);
      video.init( this.video );
      player.init();
      controls.init();
      app.animate();
      app.start();
    },
    animate: function(){
      requestAnimationFrame( app.animate );
      app.render();
    },
    start: function(){
      video.play();
      player.animate();
    },
    render: function(){
      player.render();
    }
  };
  return app;
});

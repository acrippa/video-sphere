define(['utils', 'video', 'three'], function(utils, video, THREE){
  var player = {

    _latitude: 0,
    _longitude: 0,

    _scene: new THREE.Scene(),

    init: function(){
      // CAMERA
      player.SCREEN_WIDTH = window.innerWidth, player.SCREEN_HEIGHT = window.innerHeight - 4;
      var VIEW_ANGLE = 100, ASPECT = player.SCREEN_WIDTH / player.SCREEN_HEIGHT, NEAR = 0.1, FAR = 10000;
      player._camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
      player._camera.target = new THREE.Vector3(0, 0, 0);
      player._camera.position.z = 30;

      // RENDERER
      player._renderer = utils.webglEnabled() ? new THREE.WebGLRenderer({antialias: true}) : new THREE.CanvasRenderer();
      player._renderer.setSize(player.SCREEN_WIDTH, player.SCREEN_HEIGHT);
      document.body.appendChild( player._renderer.domElement );

      player._videoImage = document.createElement( 'canvas' );
      player._videoImage.width = player.SCREEN_WIDTH;
      player._videoImage.height = player.SCREEN_HEIGHT;
      player._videoImageContext = player._videoImage.getContext( '2d' );

      player._videoTexture = new THREE.Texture( player._videoImage );
      player._videoTexture.minFilter = THREE.LinearFilter;
      player._videoTexture.magFilter = THREE.LinearFilter;
      player._movieMaterial = new THREE.MeshBasicMaterial( { map: player._videoTexture, overdraw: true, side:THREE.DoubleSide } );
      player._movieGeometry = new THREE.SphereGeometry( 1920, 100, 100 );
      player._movieScreen = new THREE.Mesh( player._movieGeometry, player._movieMaterial );
      player._movieScreen.scale.x = -1;
      player._scene.add(player._movieScreen);

      window.addEventListener( 'resize', player.resize, false );
      return this;
    },

    resize: function(){
      player.SCREEN_WIDTH = window.innerWidth, player.SCREEN_HEIGHT = window.innerHeight - 4;
      player._camera.aspect = player.SCREEN_WIDTH / player.SCREEN_HEIGHT;
      player._camera.updateProjectionMatrix();
      player._renderer.setSize( player.SCREEN_WIDTH, player.SCREEN_HEIGHT );
      player._videoImage.width = player.SCREEN_WIDTH;
      player._videoImage.height = player.SCREEN_HEIGHT;
    },

    animate: function(){
      requestAnimationFrame( player.animate );
      player.render();
    },

    render: function(){
      if ( video.ready() ) 
      {
        player._videoImageContext.drawImage( video.video(), 0, 0, player.SCREEN_WIDTH, player.SCREEN_HEIGHT );
        if ( player._videoTexture ) 
          player._videoTexture.needsUpdate = true;
      }

      // limiting latitude from -85 to 85 (cannot point to the sky or under your feet)
      player._latitude = Math.max(-85, Math.min(85, player._latitude));

      // moving the camera according to current latitude (vertical movement) and longitude (horizontal movement)
      player._camera.target.x = 500 * Math.sin(THREE.Math.degToRad(90 - player._latitude)) * Math.cos(THREE.Math.degToRad(player._longitude));
      player._camera.target.y = 500 * Math.cos(THREE.Math.degToRad(90 - player._latitude));
      player._camera.target.z = 500 * Math.sin(THREE.Math.degToRad(90 - player._latitude)) * Math.sin(THREE.Math.degToRad(player._longitude));
      player._camera.lookAt(player._camera.target);
      player._renderer.render( player._scene, player._camera );
    },

    setCoordinates: function(lat, lon){
      player._latitude = lat;
      player._longitude = lon;
    },

    getCoordinates: function(){
      return {
        latitude: player._latitude,
        longitude: player._longitude
      };
    }
  }
  return player;
});

define(['utils', 'hls', 'dash'], function(utils, Hls){
  var video = {

    init: function(options){
      this._videoElement = document.createElement( 'video' );
      this._videoElement.setAttribute("webkit-playsinline",true);
      if(!utils.isIOS() && options.src.match(/m3u8/g)){
        this._hls = new Hls();
        this._hls.loadSource(options.src);
        this._hls.attachMedia(this._videoElement);
        this._hls.on(Hls.Events.MANIFEST_PARSED,function() {
          video._loaded = true;
        });
      }
      else if (options.src.match(/mpd$/g)){
        var dash = dashjs.MediaPlayer().create();
        dash.initialize(this._videoElement, options.src, true);
        this._loaded = true;
      }else{
        this._videoElement.src = options.src;
        this._videoElement.load();
        this._loaded = true;
      }
      this._videoElement.preload=("auto");
      this._videoElement.addEventListener("loadeddata",function(){
        video._loaded=true;
      });
      this._videoElement.addEventListener("loadedmetadata",function(){
        video._loaded=true;
      });
      return this;
    },

    play: function(){
      this._loaded && this._videoElement.play();
    },

    ready: function(){
      return this._videoElement.readyState === this._videoElement.HAVE_ENOUGH_DATA;
    },

    video: function(){
      return this._videoElement;
    }

  };
  return video;
})
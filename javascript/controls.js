define(['player'], function(player){
  var controls = {
  _manualControl: false,

  init: function(player){
    document.addEventListener("mousedown", this.mouseDown, false);
    document.addEventListener("mousemove", this.mouseMove, false);
    document.addEventListener("mouseup", this.mouseUp, false);
    return this;
  },

  mouseDown: function(event){

    event.preventDefault();

    controls._manualControl = true;

    controls._savedX = event.clientX;
    controls._savedY = event.clientY;

    controls._coordinates = player.getCoordinates();

  },

  mouseMove: function(event){

    if(controls._manualControl){
      player.setCoordinates((event.clientY - controls._savedY) * 0.1 + controls._coordinates.latitude, (controls._savedX - event.clientX) * 0.1 + controls._coordinates.longitude);
    }

  },

  mouseUp: function(event){

    controls._manualControl = false;

  }
};
return controls;
})

/*
controls = function(player){
  document.addEventListener("mousedown", this.mouseDown, false);
  document.addEventListener("mousemove", this.mouseMove, false);
  document.addEventListener("mouseup", this.mouseUp, false);
  this._manualControl = false;

  this.mouseDown = function(event){

    event.preventDefault();

    this._manualControl = true;

    this._savedX = event.clientX;
    this._savedY = event.clientY;

    this._coordinates = player.getCoordinates();

  },

  this.mouseMove = function(event){

    if(this._manualControl){
      player.setCoordinates((event.clientY - savedY) * 0.1 + this._coordinates.latitude, (savedX - event.clientX) * 0.1 + this._coordinates.longitude);
    }

  },

  this.mouseUp = function(event){

    this._manualControl = false;

  }
}
*/